// MONGO CONFIG
export const MONGO_COUNTRIES_COLLECTION: string = "Tari";
export const MONGO_CITIES_COLLECTION: string = "Orase";
export const MONGO_TEMPERATURES_COLLECTION: string = "Temperaturi";

// APP PORT
export const PORT: number = 6000;