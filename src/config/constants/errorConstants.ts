export const INVALID_DATA: string = "invalid_data_400";
export const INVALID_DATA_MSG: string = "Invalid data!";

export const INVALID_ID: string = "invaliud_id_404";
export const INVALID_ID_MSG: string = "Invalid id!";

export const NAME_CONFLICT: string = "name_conflict_409";
export const NAME_CONFLICT_MSG: string = "Name already exists!";

export const NAME_OR_ID_CONFLICT: string = "name_or_idconflict_409";
export const NAME_OR_ID_CONFLICT_MSG: string = "Name or id already exists!";

export const ID_CONFLICT: string = "id_conflict_409";
export const ID_CONFLICT_MSG: string = "Id already exists!";

export const ID_NAME_CONFLICT: string = "id_name_conflict_409";
export const ID_NAME_CONFLICT_MSG: string = "Id+name already exists!";

export const ID_CID_NAME_CONFLICT: string = "id_name_conflict_409";
export const ID_CID_NAME_CONFLICT_MSG: string = "Id, countryId or name already exists!";