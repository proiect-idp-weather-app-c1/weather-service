import { TemperaturesRepository } from './temperaturesRepository';
import express from 'express';
import { Temperature } from './temperature';
import * as errorConstants from '../config/constants/errorConstants';

export class TemperaturesHandler {
    private temperaturesRepository: TemperaturesRepository;

    constructor(temperaturesRepository: TemperaturesRepository) {
        this.temperaturesRepository = temperaturesRepository;
    }

    // async handler for get
    async getTemperatures(request: express.Request, response: express.Response) {
        try {
            let lat = null, lon = null, from = null, until = null;

            if (request.query.lat !== undefined && typeof request.query.lat === "string") {
                lat = Number(request.query.lat);
            }

            if (request.query.lon !== undefined && typeof request.query.lon === "string") {
                lon = Number(request.query.lon);
            }

            if (request.query.from !== undefined && typeof request.query.from === "string") {
                from = new Date(request.query.from);
            }
            
            if (request.query.until !== undefined && typeof request.query.until === "string") {
                until = new Date(request.query.until);
            }

            let temperatures = await this.temperaturesRepository.readTemperaturesQuery(lat, lon, from, until);

            response.status(200);
            response.send(temperatures);
        } catch (err) {
            response.sendStatus(500);
        }
    }

    async getTemperaturesByCities(request: express.Request, response: express.Response) {
        try {
            let from = null, until = null;

            if (request.query.from !== undefined && typeof request.query.from === "string") {
                from = new Date(request.query.from);
            }
            
            if (request.query.until !== undefined && typeof request.query.until === "string") {
                until = new Date(request.query.until);
            }

            let temperatures = await this.temperaturesRepository.readTemperaturesByCity(
                parseInt(request.params.cityId),
                from,
                until
            );

            response.status(200);
            response.send(temperatures);
        } catch (err) {
            response.sendStatus(500);
        }
    }

    async getTemperaturesByCountries(request: express.Request, response: express.Response) {
        try {
            let from = null, until = null;

            if (request.query.from !== undefined && typeof request.query.from === "string") {
                from = new Date(request.query.from);
            }
            
            if (request.query.until !== undefined && typeof request.query.until === "string") {
                until = new Date(request.query.until);
            }

            let temperatures = await this.temperaturesRepository.readTemperaturesByCountry(
                parseInt(request.params.countryId),
                from,
                until
            );

            response.status(200);
            response.send(temperatures);
        } catch (err) {
            response.sendStatus(500);
        }
    }

    // async handler for post
    async postTemperatures(request: express.Request, response: express.Response) {
        try {
            let result = await this.temperaturesRepository.createTemperature(
                request.body.idOras,
                request.body.valoare,
            );

            if (result === "duplicate") {
                response.status(409);
                response.send({
                    message: errorConstants.NAME_CONFLICT_MSG
                });
                return;
            }

            response.status(201);
            response.send({
                id: result
            });
        } catch (err) {
            response.sendStatus(500);
        }
    }

    // async handler for put
    async putTemperatures(request: express.Request, response: express.Response) {
        try {
            let result = await this.temperaturesRepository.updateTemperature(
                parseInt(request.params.id),
                new Temperature(
                    parseInt(request.body.id),
                    request.body.idOras,
                    request.body.valoare,
                    Date.now()
                )
            );

            if (result === "duplicate") {
                response.status(409);
                response.send({
                    message: errorConstants.NAME_OR_ID_CONFLICT_MSG
                });
                return;
            }

            response.status(200);
            response.send(result);
        } catch (err) {
            response.sendStatus(500);
        }
    }

    // async handler for delete
    async deleteTemperature(request: express.Request, response: express.Response) {
        try {
            await this.temperaturesRepository.deleteTemperature(parseInt(request.params.id));

            response.status(200);
            response.send({
                "message": "Success"
            });
        } catch (err) {
            response.sendStatus(500);
        }
    }

    // handler for route not found
    routeNotFound(request: express.Request, response: express.Response) {
        response.status(505);
        response.send({
            "message": "Route not found!"
        });
    }
}