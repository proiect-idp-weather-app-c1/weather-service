import { Temperature } from './temperature';
import * as config from '../config/config';
import { Fetcher } from '../fetcher/fetcher';

export class TemperaturesRepository {
    private temperatures: Temperature[];
    private readonly fetcher: Fetcher;

    constructor(fetcher: Fetcher) {
        this.temperatures = [];
        this.fetcher = fetcher;
    }

    // creating temperature and add it to "temperatures"
    async createTemperature(cityId: number, value: number) {
        // get the max id from cities db
        let result = await this.fetcher.getFetch("http://io_service:8000/db/getMaxId?collection=" + config.MONGO_TEMPERATURES_COLLECTION);

        let maxId;

        if (result.length === 0) {
            maxId = -1;
        } else {
            maxId = result[0].id;
        }

        let documentToInsert = {
            "id": parseInt(maxId + 1),
            "valoare": value,
            "timestamp": Date.now(),
            "id_oras": cityId
        };

        const dbResult: any = await this.fetcher.postFetch("http://io_service:8000/db/insertDocument", {
            "collection": config.MONGO_TEMPERATURES_COLLECTION,
            "document": documentToInsert
        });

        if (dbResult == "duplicate") {
            return dbResult;
        }

        return maxId + 1;
    }

    // read temperatures from DB and store them in "temperatures"
    async readTemperatures() {
        let filter = {};

        let rawTemperatures: any = await this.fetcher.getFetch("http://io_service:8000/db/getDocuments?collection=" + 
            config.MONGO_TEMPERATURES_COLLECTION + "&filters=" + JSON.stringify(filter));

        this.temperatures = [];

        for (let i = 0; i < rawTemperatures.length; i++) {
            let currentTemperatures = rawTemperatures[i];

            this.temperatures.push(
                new Temperature(
                    currentTemperatures.id,
                    currentTemperatures.id_oras,
                    currentTemperatures.valoare,
                    currentTemperatures.timestamp
                )
            );
        }

        return this.temperatures;
    }

    // read temperatures from DB by query
    async readTemperaturesQuery(lat: number | null, lon: number | null, from: Date | null, until: Date | null) {
        let filter;

        if (lat != null && lon != null) {
            filter = {
                latitudine: lat,
                longitudine: lon
            };
        } else if (lat != null && lon == null) {
            filter = {
                latitudine: lat
            };
        } else if (lat == null && lon != null) {
            filter = {
                longitudine: lon
            };
        } else {
            filter = {};
        }

        let rawCities: any = await this.fetcher.getFetch("http://io_service:8000/db/getDocuments?collection=" + 
            config.MONGO_CITIES_COLLECTION + "&filters=" + JSON.stringify(filter));

        let citiesIds = [];

        for (let i = 0; i < rawCities.length; i++) {
            let currentCity = rawCities[i];

            citiesIds.push(currentCity.id);
        }
        
        let newFilter;

        if (from != null && until != null) {
            newFilter = {
                id_oras: {
                    $in: citiesIds
                },
                timestamp: {
                    $gte: from.valueOf(),
                    $lte: until.valueOf()
                }
            };
        } else if (from != null && until == null) {
            newFilter = {
                id_oras: {
                    $in: citiesIds
                },
                timestamp: {
                    $gte: from.valueOf()
                }
            };
        } else if (from == null && until != null) {
            newFilter = {
                id_oras: {
                    $in: citiesIds
                },
                timestamp: {
                    $lte: until.valueOf()
                }
            };
        } else {
            newFilter = {
                id_oras: {
                    $in: citiesIds
                }
            };
        }

        let rawTemperatures: any = await this.fetcher.getFetch("http://io_service:8000/db/getDocuments?collection=" + 
            config.MONGO_TEMPERATURES_COLLECTION + "&filters=" + JSON.stringify(newFilter));

        let temperatures = [];

        for (let i = 0; i < rawTemperatures.length; i++) {
            let currentTemperatures = rawTemperatures[i];

            temperatures.push({
                id: currentTemperatures.id,
                valoare: currentTemperatures.valoare,
                timestamp: new Date(currentTemperatures.timestamp)
            });
        }

        return temperatures;
    }

    // read temperatures by city from DB
    async readTemperaturesByCity(cityId: number, from: Date | null, until: Date | null) {
        let filter;
        
        if (from != null && until != null) {
            filter = {
                id_oras: cityId,
                timestamp: {
                    $gte: from.valueOf(),
                    $lte: until.valueOf()
                }
            };
        } else if (from != null && until == null) {
            filter = {
                id_oras: cityId,
                timestamp: {
                    $gte: from.valueOf()
                }
            };
        } else if (from == null && until != null) {
            filter = {
                id_oras: cityId,
                timestamp: {
                    $lte: until.valueOf()
                }
            };
        } else {
            filter = {
                id_oras: cityId
            };
        }

        let rawTemperatures: any = await this.fetcher.getFetch("http://io_service:8000/db/getDocuments?collection=" + 
            config.MONGO_TEMPERATURES_COLLECTION + "&filters=" + JSON.stringify(filter));

        let temperatures = [];

        for (let i = 0; i < rawTemperatures.length; i++) {
            let currentTemperatures = rawTemperatures[i];

            temperatures.push({
                id: currentTemperatures.id,
                valoare: currentTemperatures.valoare,
                timestamp: new Date(currentTemperatures.timestamp)
            });
        }

        return temperatures;
    }

    // read temperatures by country from DB
    async readTemperaturesByCountry(countryId: number, from: Date | null, until: Date | null) {
        let filter = {
            id_tara: countryId
        };

        let rawCities: any = await this.fetcher.getFetch("http://io_service:8000/db/getDocuments?collection=" + 
            config.MONGO_CITIES_COLLECTION + "&filters=" + JSON.stringify(filter));

        let citiesIds: Array<number> = [];

        for (let i = 0; i < rawCities.length; i++) {
            let currentCity = rawCities[i];

            citiesIds.push(currentCity.id);
        }

        let newFilter;

        if (from != null && until != null) {
            newFilter = {
                id_oras: {
                    $in: citiesIds
                },
                timestamp: {
                    $gte: from.valueOf(),
                    $lte: until.valueOf()
                }
            };
        } else if (from != null && until == null) {
            newFilter = {
                id_oras: {
                    $in: citiesIds
                },
                timestamp: {
                    $gte: from.valueOf()
                }
            };
        } else if (from == null && until != null) {
            newFilter = {
                id_oras: {
                    $in: citiesIds
                },
                timestamp: {
                    $lte: until.valueOf()
                }
            };
        } else {
            newFilter = {
                id_oras: {
                    $in: citiesIds
                },
            };
        }

        let rawTemperatures: any = await this.fetcher.getFetch("http://io_service:8000/db/getDocuments?collection=" + 
            config.MONGO_TEMPERATURES_COLLECTION + "&filters=" + JSON.stringify(newFilter));

        let temperatures = [];

        for (let i = 0; i < rawTemperatures.length; i++) {
            let currentTemperatures = rawTemperatures[i];

            temperatures.push({
                id: currentTemperatures.id,
                valoare: currentTemperatures.valoare,
                timestamp: new Date(currentTemperatures.timestamp)
            });
        }

        return temperatures;
    }

    // update an already existing temperature
    async updateTemperature(oldId: number, newTemperature: Temperature) {
        let filters = {
            "id": oldId
        };

        let dataToUpdate = {
            "id": newTemperature.getId(),
            "valoare": newTemperature.getValue(),
            "timestamp": newTemperature.getTimestamp(),
            "id_oras": newTemperature.getCityId(),
        };

        const dbResult: any = await this.fetcher.putFetch("http://io_service:8000/db/updateDocument", {
            "collection": config.MONGO_TEMPERATURES_COLLECTION,
            "filters": filters,
            "document": dataToUpdate
        });

        if (dbResult == "duplicate") {
            return dbResult;
        }

        return newTemperature;
    }

    // delete an already existing temperature
    async deleteTemperature(id: number) {
        let filters = {
            "id": id
        };

        await this.fetcher.deleteFetch("http://io_service:8000/db/deleteDocument?collection=" + 
            config.MONGO_TEMPERATURES_COLLECTION + "&filters=" + JSON.stringify(filters));

        return id;
    }

    getTemperatures(): Temperature[] {
        return this.temperatures;
    }
}