export class Temperature {
    private id: number;
    private cityId: number;
    private value: number;
    private timestamp: number;

    constructor(id: number, cityId: number, value: number, timestamp: number) {
        this.id = id;
        this.cityId = cityId;
        this.value = value;
        this.timestamp = timestamp;
    }

    public getId(): number {
        return this.id;
    }

    public setId(id: number): void  {
        this.id = id;
    }

    public getCityId(): number {
        return this.cityId;
    }

    public setCityId(cityId: number): void  {
        this.cityId = cityId;
    }

    public getValue(): number {
        return this.value;
    }

    public setValue(value: number): void {
        this.value = value;
    }

    public getTimestamp(): number {
        return this.timestamp;
    }

    public setTimestamp(timestamp: number): void {
        this.timestamp = timestamp;
    }

    public toJSON() {
        return {
            id: this.id,
            idOras: this.cityId,
            valoare: this.value,
            timestamp: new Date(this.timestamp),
        }
    }
}