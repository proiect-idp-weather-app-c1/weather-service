import { CitiesRepository } from './citiesRepository';
import express from 'express';
import { City } from './city';
import * as errorConstants from '../config/constants/errorConstants';

export class CitiesHandler {
    private citiesRepository: CitiesRepository;

    constructor(citiesRepository: CitiesRepository) {
        this.citiesRepository = citiesRepository;
    }

    // async handler for get
    async getCities(request: express.Request, response: express.Response) {
        try {
            let cities = await this.citiesRepository.readCities();

            response.status(200);
            response.send(cities);
        } catch (err) {
            response.sendStatus(500);
        }
    }

    // async handler for get
    async getCitiesByCountry(request: express.Request, response: express.Response) {
        try {
            let cities = await this.citiesRepository.readCitiesByCountry(parseInt(request.params.countryId));

            response.status(200);
            response.send(cities);
        } catch (err) {
            response.sendStatus(500);
        }
    }

    // async handler for post
    async postCities(request: express.Request, response: express.Response) {
        try {
            let result = await this.citiesRepository.createCity(
                parseInt(request.body.idTara),
                request.body.nume,
                request.body.lat,
                request.body.lon
            );

            if (result === "duplicate") {
                response.status(409);
                response.send({
                    message: errorConstants.ID_NAME_CONFLICT_MSG
                });
                return;
            }

            response.status(201);
            response.send({
                id: result
            });
        } catch (err) {
            response.sendStatus(500);
        }
    }

    // async handler for put
    async putCities(request: express.Request, response: express.Response) {
        try {
            let result = await this.citiesRepository.updateCity(
                parseInt(request.params.id),
                new City(
                    parseInt(request.body.id),
                    parseInt(request.body.idTara),
                    request.body.nume,
                    request.body.lat,
                    request.body.lon
                )
            );

            if (result === "duplicate") {
                response.status(409);
                response.send({
                    message: errorConstants.ID_CID_NAME_CONFLICT_MSG
                });
                return;
            }

            response.status(200);
            response.send(result);
        } catch (err) {
            response.sendStatus(500);
        }
    }

    // async handler for delete
    async deleteCities(request: express.Request, response: express.Response) {
        try {
            await this.citiesRepository.deleteCity(parseInt(request.params.id));

            response.status(200);
            response.send({
                "message": "Success"
            });
        } catch (err) {
            response.sendStatus(500);
        }
    }

    // handler for route not found
    routeNotFound(request: express.Request, response: express.Response) {
        response.status(505);
        response.send({
            "message": "Route not found!"
        });
    }
}