export class City {
    private id: number;
    private countryId: number;
    private name: string;
    private latitude: number;
    private longitude: number;

    constructor(id: number, countryId: number, name: string, latitude: number, longitude: number) {
        this.id = id;
        this.countryId = countryId;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public getId(): number {
        return this.id;
    }

    public setId(id: number): void  {
        this.id = id;
    }

    public getCountryId(): number {
        return this.countryId;
    }

    public setCountryId(countryId: number): void  {
        this.countryId = countryId;
    }

    public getName(): string {
        return this.name;
    }

    public setName(name: string): void {
        this.name = name;
    }

    public getLatitude(): number {
        return this.latitude;
    }

    public setLatitude(latitude: number): void  {
        this.latitude = latitude;
    }

    public getLongitude(): number {
        return this.longitude;
    }

    public setLongitude(longitude: number): void  {
        this.longitude = longitude;
    }

    public toJSON() {
        return {
            id: this.id,
            idTara: this.countryId,
            nume: this.name,
            lat: this.latitude,
            lon: this.longitude
        }
    }
}