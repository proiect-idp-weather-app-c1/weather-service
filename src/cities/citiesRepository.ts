import { City } from './city';
import * as config from '../config/config';
import { Fetcher } from '../fetcher/fetcher';

export class CitiesRepository {
    private cities: City[];
    private readonly fetcher: Fetcher;

    constructor(fetcher: Fetcher) {
        this.cities = [];
        this.fetcher = fetcher;
    }

    // creating city and add it to "cities"
    async createCity(countryId: number, name: string, latitude: number, longitude: number) {
        // get the max id from cities db
        let result = await this.fetcher.getFetch("http://io_service:8000/db/getMaxId?collection=" + config.MONGO_CITIES_COLLECTION);

        let maxId;

        if (result.length === 0) {
            maxId = -1;
        } else {
            maxId = result[0].id;
        }

        let documentToInsert = {
            "id": parseInt(maxId + 1),
            "id_tara": countryId,
            "nume_oras": name,
            "latitudine": latitude,
            "longitudine": longitude
        };

        const dbResult: any = await this.fetcher.postFetch("http://io_service:8000/db/insertDocument", {
            "collection": config.MONGO_CITIES_COLLECTION,
            "document": documentToInsert
        });

        if (dbResult == "duplicate") {
            return dbResult;
        }

        return maxId + 1;
    }

    // read cities from DB and store them in "cities"
    async readCities() {
        let filter = {};

        let rawCities: any = await this.fetcher.getFetch("http://io_service:8000/db/getDocuments?collection=" + 
            config.MONGO_CITIES_COLLECTION + "&filters=" + JSON.stringify(filter));

        this.cities = [];

        for (let i = 0; i < rawCities.length; i++) {
            let currentCity = rawCities[i];

            this.cities.push(
                new City(
                    currentCity.id,
                    currentCity.id_tara,
                    currentCity.nume_oras,
                    currentCity.latitudine,
                    currentCity.longitudine
                )
            );
        }

        return this.cities;
    }

    // read cities from DB and store them in "cities"
    async readCitiesByCountry(countryId: number) {
        let filter = {
            id_tara: countryId
        };

        let rawCities: any = await this.fetcher.getFetch("http://io_service:8000/db/getDocuments?collection=" + 
            config.MONGO_CITIES_COLLECTION + "&filters=" + JSON.stringify(filter));

        this.cities = [];

        for (let i = 0; i < rawCities.length; i++) {
            let currentCity = rawCities[i];

            this.cities.push(
                new City(
                    currentCity.id,
                    currentCity.id_tara,
                    currentCity.nume_oras,
                    currentCity.latitudine,
                    currentCity.longitudine
                )
            );
        }

        return this.cities;
    }


    // update an already existing city
    async updateCity(oldId: number, newCity: City) {
        let filters = {
            "id": oldId
        };

        let dataToUpdate = {
            "id": newCity.getId(),
            "id_tara": newCity.getCountryId(),
            "nume_oras": newCity.getName(),
            "latitudine": newCity.getLatitude(),
            "longitudine": newCity.getLongitude()
        };

        const dbResult: any = await this.fetcher.putFetch("http://io_service:8000/db/updateDocument", {
            "collection": config.MONGO_CITIES_COLLECTION,
            "filters": filters,
            "document": dataToUpdate
        });

        if (dbResult == "duplicate") {
            return dbResult;
        }

        return newCity;
    }

    // delete an already existing city
    async deleteCity(id: number) {
        let filters = {
            "id": id
        };

        let filtersForTemperatures = {
            "id_oras": id
        };

        await this.fetcher.deleteFetch("http://io_service:8000/db/deleteDocument?collection=" + 
            config.MONGO_TEMPERATURES_COLLECTION + "&filters=" + JSON.stringify(filtersForTemperatures));

        await this.fetcher.deleteFetch("http://io_service:8000/db/deleteDocument?collection=" + 
            config.MONGO_CITIES_COLLECTION + "&filters=" + JSON.stringify(filters));

        return id;
    }

    getCities(): City[] {
        return this.cities;
    }
}