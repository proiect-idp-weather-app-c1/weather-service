import axios from 'axios';

export class Fetcher {

    async getFetch(url: string) {
        const response = await axios.get(url);

        return response.data;
    }

    async postFetch(url: string, data: any) {
        const response = await axios.post(url, data);

        return response.data;
    }

    async putFetch(url: string, data: any) {
        const response = await axios.put(url, data);

        return response.data;
    }

    async deleteFetch(url: string) {
        const response = await axios.delete(url);

        return response.data;
    }
}