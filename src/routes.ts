import express from 'express';
import bodyParser from 'body-parser';
import * as config from './config/config';
import { ErrorValidator } from './validators/errorValidator';
import { CountriesRepository } from './countries/countriesRepository';
import { CountriesHandler } from './countries/countriesHandler';
import { CountriesRequestValidator } from './validators/countriesRequestValidator';
import { CitiesHandler } from './cities/citiesHandler';
import { CitiesRepository } from './cities/citiesRepository';
import { CitiesRequestValidator } from './validators/citiesRequestValidator';
import { TemperaturesRepository } from './temperatures/temperaturesRepository';
import { TemperaturesHandler } from './temperatures/temperaturesHandler';
import { TemperaturesRequestValidator } from './validators/temperaturesRequestValidator';
import { Fetcher } from './fetcher/fetcher';

const app = express();
const port = config.PORT;

const Prometheus = require('prom-client');
const register = new Prometheus.Registry();

register.setDefaultLabels({
    app: 'app'
});

Prometheus.collectDefaultMetrics({ register })

const counter = new Prometheus.Counter({
  name: 'api_http_request_count',
  help: 'Count of HTTP requests made to API',
  labelNames: ['method', 'route', 'statusCode']
});

register.registerMetric(counter);

const auth = require("./middleware/auth");

let fetcher = new Fetcher();

let errorValidator = new ErrorValidator();

let countriesRepository = new CountriesRepository(fetcher);
let countryHandler = new CountriesHandler(countriesRepository);

let countriesRequestValidator = new CountriesRequestValidator(countriesRepository);

let citiesRepository = new CitiesRepository(fetcher);
let cityHandler = new CitiesHandler(citiesRepository);

let citiesRequestValidator = new CitiesRequestValidator(citiesRepository, countriesRepository);

let temperaturesRepository = new TemperaturesRepository(fetcher);
let temperatureHandler = new TemperaturesHandler(temperaturesRepository);

let temperaturesRequestValidator = new TemperaturesRequestValidator(temperaturesRepository, citiesRepository, countriesRepository);


app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(errorValidator.headers);

// ROUTES FOR COUNTRIES
app.get('/api/countries', auth,
    async (request: express.Request, response: express.Response) => {
        await countryHandler.getCountries(request, response);
        counter.labels({method: request.method, route: '/api/countries', statusCode: response.statusCode}).inc();
    }
);

app.post('/api/countries', auth,
    countriesRequestValidator.postParameterValidation.bind(countriesRequestValidator),
    async (request: express.Request, response: express.Response) => {
        await countryHandler.postCountries(request, response);
        counter.labels({method: request.method, route: '/api/countries', statusCode: response.statusCode}).inc();
    }
);

app.put('/api/countries/:id', auth,
    countriesRequestValidator.putParameterValidation.bind(countriesRequestValidator),
    async (request: express.Request, response: express.Response) => {
        await countryHandler.putCountries(request, response);
        counter.labels({method: request.method, route: '/api/countries', statusCode: response.statusCode}).inc();
    }
);

app.delete('/api/countries/:id', auth,
    countriesRequestValidator.deleteParameterValidation.bind(countriesRequestValidator),
    async (request: express.Request, response: express.Response) => {
        await countryHandler.deleteCountries(request, response);
        counter.labels({method: request.method, route: '/api/countries', statusCode: response.statusCode}).inc();
    }
);

// ROUTES FOR CITIES
app.get('/api/cities', auth,
    async (request: express.Request, response: express.Response) => {
        await cityHandler.getCities(request, response);
        counter.labels({method: request.method, route: '/api/cities', statusCode: response.statusCode}).inc();
    }
);

app.get('/api/cities/country/:countryId', auth,
    async (request: express.Request, response: express.Response) => {
        await cityHandler.getCitiesByCountry(request, response);
        counter.labels({method: request.method, route: '/api/cities', statusCode: response.statusCode}).inc();
    }
);

app.post('/api/cities', auth,
    citiesRequestValidator.postParameterValidation.bind(citiesRequestValidator),
    async (request: express.Request, response: express.Response) => {
        await cityHandler.postCities(request, response);
        counter.labels({method: request.method, route: '/api/cities', statusCode: response.statusCode}).inc();
    }
);

app.put('/api/cities/:id', auth,
    citiesRequestValidator.putParameterValidation.bind(citiesRequestValidator),
    async (request: express.Request, response: express.Response) => {
        await cityHandler.putCities(request, response);
        counter.labels({method: request.method, route: '/api/cities', statusCode: response.statusCode}).inc();
    }
);

app.delete('/api/cities/:id', auth,
    citiesRequestValidator.deleteParameterValidation.bind(citiesRequestValidator),
    async (request: express.Request, response: express.Response) => {
        await cityHandler.deleteCities(request, response);
        counter.labels({method: request.method, route: '/api/cities', statusCode: response.statusCode}).inc();
    }
);

// ROUTES FOR TEMPERATURES
app.get('/api/temperatures', auth,
    async (request: express.Request, response: express.Response) => {
        await temperatureHandler.getTemperatures(request, response);
        counter.labels({method: request.method, route: '/api/temperatures', statusCode: response.statusCode}).inc();
    }
);

app.get('/api/temperatures/cities/:cityId', auth,
    temperaturesRequestValidator.getByCityParameterValidation.bind(temperaturesRequestValidator),
    async (request: express.Request, response: express.Response) => {
        await temperatureHandler.getTemperaturesByCities(request, response);
        counter.labels({method: request.method, route: '/api/temperatures', statusCode: response.statusCode}).inc();
    }
);

app.get('/api/temperatures/countries/:countryId', auth,
    temperaturesRequestValidator.getByCountryParameterValidation.bind(temperaturesRequestValidator),
    async (request: express.Request, response: express.Response) => {
        await temperatureHandler.getTemperaturesByCountries(request, response);
        counter.labels({method: request.method, route: '/api/temperatures', statusCode: response.statusCode}).inc();
    }
);

app.post('/api/temperatures', auth,
    temperaturesRequestValidator.postParameterValidation.bind(temperaturesRequestValidator),
    async (request: express.Request, response: express.Response) => {
        await temperatureHandler.postTemperatures(request, response);
        counter.labels({method: request.method, route: '/api/temperatures', statusCode: response.statusCode}).inc();
    }
);

app.put('/api/temperatures/:id', auth,
    temperaturesRequestValidator.putParameterValidation.bind(temperaturesRequestValidator),
    async (request: express.Request, response: express.Response) => {
        await temperatureHandler.putTemperatures(request, response);
        counter.labels({method: request.method, route: '/api/temperatures', statusCode: response.statusCode}).inc();
    }
);

app.delete('/api/temperatures/:id', auth,
    temperaturesRequestValidator.deleteParameterValidation.bind(temperaturesRequestValidator),
    async (request: express.Request, response: express.Response) => {
        await temperatureHandler.deleteTemperature(request, response);
        counter.labels({method: request.method, route: '/api/temperatures', statusCode: response.statusCode}).inc();
    }
);

app.get('/metrics', async (req, res) => {
    res.setHeader('Content-Type', register.contentType);
    res.send(await register.metrics());
})
// middleware for error handling
app.use(errorValidator.errorHandling);

// listen on a specific port
app.listen(port, () => {
    console.log('Application listening on port ' + port + " !");
});