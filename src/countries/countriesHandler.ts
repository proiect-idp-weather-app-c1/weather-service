import { CountriesRepository } from './countriesRepository';
import express from 'express';
import { Country } from './country';
import * as errorConstants from '../config/constants/errorConstants';

export class CountriesHandler {
    private countriesRepository: CountriesRepository;

    constructor(countriesRepository: CountriesRepository) {
        this.countriesRepository = countriesRepository;
    }

    // async handler for get
    async getCountries(request: express.Request, response: express.Response) {
        try {
            let countries = await this.countriesRepository.readCountries();

            response.status(200);
            response.send(countries);
        } catch (err) {
            response.sendStatus(500);
        }
    }

    // async handler for post
    async postCountries(request: express.Request, response: express.Response) {
        try {
            let result = await this.countriesRepository.createCountry(
                request.body.nume,
                request.body.lat,
                request.body.lon
            );

            if (result === "duplicate") {
                response.status(409);
                response.send({
                    message: errorConstants.NAME_CONFLICT_MSG
                });
                return;
            }

            response.status(201);
            response.send({
                id: result
            });
        } catch (err) {
            response.sendStatus(500);
        }
    }

    // async handler for put
    async putCountries(request: express.Request, response: express.Response) {
        try {
            let result = await this.countriesRepository.updateCountry(
                parseInt(request.params.id),
                new Country(
                    parseInt(request.body.id),
                    request.body.nume,
                    request.body.lat,
                    request.body.lon
                )
            );

            if (result === "duplicate") {
                response.status(409);
                response.send({
                    message: errorConstants.NAME_OR_ID_CONFLICT_MSG
                });
                return;
            }

            response.status(200);
            response.send(result);
        } catch (err) {
            response.sendStatus(500);
        }
    }

    // async handler for delete
    async deleteCountries(request: express.Request, response: express.Response) {
        try {
            await this.countriesRepository.deleteCountry(parseInt(request.params.id));

            response.status(200);
            response.send({
                "message": "Success"
            });
        } catch (err) {
            response.sendStatus(500);
        }
    }

    // handler for route not found
    routeNotFound(request: express.Request, response: express.Response) {
        response.status(505);
        response.send({
            "message": "Route not found!"
        });
    }
}