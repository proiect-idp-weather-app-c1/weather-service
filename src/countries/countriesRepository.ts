import { Country } from './country';
import * as config from '../config/config';
import { Fetcher } from '../fetcher/fetcher';

export class CountriesRepository {
    private countries: Country[];
    private readonly fetcher: Fetcher;

    constructor(fetcher: Fetcher) {
        this.countries = [];
        this.fetcher = fetcher;
    }

    // creating country and add it to "countries"
    async createCountry(name: string, latitude: number, longitude: number) {
        // get the max id from countries db
        let result = await this.fetcher.getFetch("http://io_service:8000/db/getMaxId?collection=" + config.MONGO_COUNTRIES_COLLECTION);

        let maxId;

        if (result.length === 0) {
            maxId = -1;
        } else {
            maxId = result[0].id;
        }

        let documentToInsert = {
            "id": parseInt(maxId + 1),
            "nume_tara": name,
            "latitudine": latitude,
            "longitudine": longitude
        };

        const dbResult: any = await this.fetcher.postFetch("http://io_service:8000/db/insertDocument", {
            "collection": config.MONGO_COUNTRIES_COLLECTION,
            "document": documentToInsert
        });

        if (dbResult == "duplicate") {
            return dbResult;
        }

        return maxId + 1;
    }

    // read countries from DB and store them in "countries"
    async readCountries() {
        let filter = {};

        let rawCountries: any = await this.fetcher.getFetch("http://io_service:8000/db/getDocuments?collection=" + 
            config.MONGO_COUNTRIES_COLLECTION + "&filters=" + JSON.stringify(filter));

        this.countries = [];

        for (let i = 0; i < rawCountries.length; i++) {
            let currentCountry = rawCountries[i];

            this.countries.push(
                new Country(
                    currentCountry.id,
                    currentCountry.nume_tara,
                    currentCountry.latitudine,
                    currentCountry.longitudine
                )
            );
        }

        return this.countries;
    }

    // update an already existing country
    async updateCountry(oldId: number, newCountry: Country) {
        let filters = {
            "id": oldId
        };

        let dataToUpdate = {
            "id": newCountry.getId(),
            "nume_tara": newCountry.getName(),
            "latitudine": newCountry.getLatitude(),
            "longitudine": newCountry.getLongitude()
        };

        const dbResult: any = await this.fetcher.putFetch("http://io_service:8000/db/updateDocument", {
            "collection": config.MONGO_COUNTRIES_COLLECTION,
            "filters": filters,
            "document": dataToUpdate
        });

        if (dbResult == "duplicate") {
            return dbResult;
        }

        return newCountry;
    }

    // delete an already existing country
    async deleteCountry(id: number) {
        let filterForCities = {
            "id_tara": id
        };

        let rawCities: any = await this.fetcher.getFetch("http://io_service:8000/db/getDocuments?collection=" + 
            config.MONGO_CITIES_COLLECTION + "&filters=" + JSON.stringify(filterForCities));

        let citiesIds = [];

        for (let i = 0; i < rawCities.length; i++) {
            let currentCity = rawCities[i];

            citiesIds.push(currentCity.id);
        }

        await this.fetcher.deleteFetch("http://io_service:8000/db/deleteDocument?collection=" + 
            config.MONGO_CITIES_COLLECTION + "&filters=" + JSON.stringify(filterForCities));

        let filtersForTemperatures = {
            id_oras: {
                $in: citiesIds
            }
        };

        await this.fetcher.deleteFetch("http://io_service:8000/db/deleteDocument?collection=" + 
            config.MONGO_TEMPERATURES_COLLECTION + "&filters=" + JSON.stringify(filtersForTemperatures));

        let filters = {
            "id": id
        };

        await this.fetcher.deleteFetch("http://io_service:8000/db/deleteDocument?collection=" + 
            config.MONGO_COUNTRIES_COLLECTION + "&filters=" + JSON.stringify(filters));

        return id;
    }
}