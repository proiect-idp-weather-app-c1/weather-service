export class Country {
    private id: number;
    private name: string;
    private latitude: number;
    private longitude: number;

    constructor(id: number, name: string, latitude: number, longitude: number) {
        this.id = id;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public getId(): number {
        return this.id;
    }

    public setId(id: number): void  {
        this.id = id;
    }

    public getName(): string {
        return this.name;
    }

    public setName(name: string): void {
        this.name = name;
    }

    public getLatitude(): number {
        return this.latitude;
    }

    public setLatitude(latitude: number): void  {
        this.latitude = latitude;
    }

    public getLongitude(): number {
        return this.longitude;
    }

    public setLongitude(longitude: number): void  {
        this.longitude = longitude;
    }

    public toJSON() {
        return {
            id: this.id,
            nume: this.name,
            lat: this.latitude,
            lon: this.longitude
        }
    }
}