import express from 'express';
import * as errorConstants from '../config/constants/errorConstants';
import { CitiesRepository } from '../cities/citiesRepository';
import { TemperaturesRepository } from '../temperatures/temperaturesRepository';
import { CountriesRepository } from '../countries/countriesRepository';

export class TemperaturesRequestValidator {
    private citiesRepository: CitiesRepository;
    private countriesRepository: CountriesRepository;
    private temperaturesRepository: TemperaturesRepository;

    constructor(temperaturesRepository: TemperaturesRepository, citiesRepository: CitiesRepository, countriesRepository: CountriesRepository) {
        this.citiesRepository = citiesRepository;
        this.temperaturesRepository = temperaturesRepository;
        this.countriesRepository = countriesRepository;
    }

    // check if id exists
    private async idExists(id: number) {
        let temperatures = await this.temperaturesRepository.readTemperatures();

        // check if id exists
        if (temperatures.filter(temperature => temperature.getId() === id).length != 0) {
            return true;
        }

        return false;
    }

    // check if cityId exists in cities
    private async cityExists(cityId: number) {
        let cities = await this.citiesRepository.readCities();

        // check if cityId exists
        if (cities.filter(city => city.getId() === cityId).length != 0) {
            return true;
        }

        return false;
    }

    // check if countryId exists in countries
    private async countryExists(countryId: number) {
        let countries = await this.countriesRepository.readCountries();

        // check if countryId exists
        if (countries.filter(country => country.getId() === countryId).length != 0) {
            return true;
        }

        return false;
    }

    // middleware validation for getByCity
    async getByCityParameterValidation(request: express.Request, response: express.Response, next: express.NextFunction) {
        // check if city exists
        if (!await this.cityExists(parseInt(request.params.cityId))) {
            // next(new Error(errorConstants.INVALID_DATA));
            response.status(200);
            response.send([]);
            return;
        }

        next();
    }

    // middleware validation for getByCountry
    async getByCountryParameterValidation(request: express.Request, response: express.Response, next: express.NextFunction) {
        // check if city exists
        if (!await this.countryExists(parseInt(request.params.countryId))) {
            // next(new Error(errorConstants.INVALID_DATA));
            response.status(200);
            response.send([]);
            return;
        }

        next();
    }

    // middleware validation for post
    async postParameterValidation(request: express.Request, response: express.Response, next: express.NextFunction) {
        // all fields are mandatory
        if (request.body.idOras == undefined ||
            request.body.valoare == undefined) {
            next(new Error(errorConstants.INVALID_DATA));
            return;
        }
        
        // check if types are correct
        if (typeof request.body.idOras !== "number" ||
            typeof request.body.valoare !== "number") {
                next(new Error(errorConstants.INVALID_DATA));
                return;
            }

        if (!await this.cityExists(request.body.idOras)) {
            next(new Error(errorConstants.INVALID_DATA));
            return;
        }

        next();
    }


    // middleware validation for put
    async putParameterValidation(request: express.Request, response: express.Response, next: express.NextFunction) {
        // check if id exists - id has to exist
        if (!await this.idExists(parseInt(request.params.id))) {
            next(new Error(errorConstants.INVALID_ID));
            return;
        }

        // validation if body fields (keys) are correct
        // also, all fields are mandatory
        if (request.body.id == undefined ||
            request.body.idOras == undefined ||
            request.body.valoare == undefined) {
                next(new Error(errorConstants.INVALID_DATA));
                return;
            }
        
        // check if types are correct
        if (typeof request.body.id !== "number" ||
            typeof request.body.idOras !== "number" ||
            typeof request.body.valoare !== "number") {
                next(new Error(errorConstants.INVALID_DATA));
                return;
            }
        
        if (parseInt(request.body.id) !== parseInt(request.params.id)) {
            next(new Error(errorConstants.INVALID_DATA));
            return;
        }

        if (!await this.cityExists(request.body.idOras)) {
            next(new Error(errorConstants.INVALID_DATA));
            return;
        }

        next();
    }

    // middleware validation for delete
    async deleteParameterValidation(request: express.Request, response: express.Response, next: express.NextFunction) {
        // check if id exists - id has to exist
        if (!await this.idExists(parseInt(request.params.id))) {
            next(new Error(errorConstants.INVALID_ID));
            return;
        }

        next();
    }
}