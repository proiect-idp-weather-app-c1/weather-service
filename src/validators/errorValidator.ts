import express from 'express';
import * as errorConstants from '../config/constants/errorConstants';

export class ErrorValidator {
    
    constructor() {

    }

    // middleware error handler
    errorHandling(error: any, request: express.Request, response: express.Response, next: express.NextFunction) {
        switch (error.message) {
            case errorConstants.INVALID_DATA:
                response.status(400).send({
                    "message": errorConstants.INVALID_DATA_MSG
                });
                break;
            case errorConstants.INVALID_ID:
                response.status(404).send({
                    "message": errorConstants.INVALID_ID_MSG
                });
                break;
            case errorConstants.NAME_CONFLICT:
                response.status(409).send({
                    "message": errorConstants.NAME_CONFLICT_MSG
                });
                break;
            case errorConstants.ID_CONFLICT:
                response.status(409).send({
                    "message": errorConstants.ID_CONFLICT_MSG
                });
                break;
            default:
                response.sendStatus(200);
                break;
        }
    }

    // middleware to allow access
    headers(request: express.Request, response: express.Response, next: express.NextFunction) {
        response.header('Access-Control-Allow-Origin', '*');
        response.header('Access-Control-Allow-Methods', 'GET,POST,PUT,DELETE');
        response.header('Access-Control-Allow-Headers', 'Content-Type');

        next();
    }
}