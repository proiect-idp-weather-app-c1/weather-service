import express from 'express';
import * as errorConstants from '../config/constants/errorConstants';
import { CitiesRepository } from '../cities/citiesRepository';
import { CountriesRepository } from '../countries/countriesRepository';

export class CitiesRequestValidator {
    private citiesRepository: CitiesRepository;
    private countriesRepository: CountriesRepository;

    constructor(citiesRepository: CitiesRepository, countriesRepository: CountriesRepository) {
        this.citiesRepository = citiesRepository;
        this.countriesRepository = countriesRepository;
    }

    // check if id exists
    private async idExists(id: number) {
        let cities = await this.citiesRepository.readCities();

        // check if id exists
        if (cities.filter(city => city.getId() === id).length != 0) {
            return true;
        }

        return false;
    }

    // check if countryId exists in countries
    private async countryExists(countryId: number) {
        let countries = await this.countriesRepository.readCountries();

        // check if countryId exists
        if (countries.filter(country => country.getId() === countryId).length != 0) {
            return true;
        }

        return false;
    }

    // middleware validation for post
    async postParameterValidation(request: express.Request, response: express.Response, next: express.NextFunction) {
        // all fields are mandatory
        if (request.body.idTara == undefined ||
            request.body.nume == undefined ||
            request.body.lat == undefined ||
            request.body.lon == undefined) {
            next(new Error(errorConstants.INVALID_DATA));
            return;
        }
        
        // check if types are correct
        if (typeof request.body.idTara !== "number" ||
            typeof request.body.nume !== "string" ||
            typeof request.body.lat !== "number" ||
            typeof request.body.lon !== "number") {
                next(new Error(errorConstants.INVALID_DATA));
                return;
            }

        if (!await this.countryExists(request.body.idTara)) {
            next(new Error(errorConstants.INVALID_DATA));
            return;
        }

        next();
    }


    // middleware validation for put
    async putParameterValidation(request: express.Request, response: express.Response, next: express.NextFunction) {
        // check if id exists - id has to exist
        if (!await this.idExists(parseInt(request.params.id))) {
            next(new Error(errorConstants.INVALID_ID));
            return;
        }

        // validation if body fields (keys) are correct
        // also, all fields are mandatory
        if (request.body.idTara == undefined ||
            request.body.id == undefined ||
            request.body.nume == undefined ||
            request.body.lat == undefined ||
            request.body.lon == undefined) {
                next(new Error(errorConstants.INVALID_DATA));
                return;
            }
        
        // check if types are correct
        if (typeof request.body.idTara !== "number" ||
            typeof request.body.id !== "number" ||
            typeof request.body.nume !== "string" ||
            typeof request.body.lat !== "number" ||
            typeof request.body.lon !== "number") {
                next(new Error(errorConstants.INVALID_DATA));
                return;
            }

        if (parseInt(request.body.id) !== parseInt(request.params.id)) {
            next(new Error(errorConstants.INVALID_DATA));
            return;
        }

        if (!await this.countryExists(request.body.idTara)) {
            next(new Error(errorConstants.INVALID_DATA));
            return;
        }

        next();
    }

    // middleware validation for delete
    async deleteParameterValidation(request: express.Request, response: express.Response, next: express.NextFunction) {
        // check if id exists - id has to exist
        if (!await this.idExists(parseInt(request.params.id))) {
            next(new Error(errorConstants.INVALID_ID));
            return;
        }

        next();
    }
}